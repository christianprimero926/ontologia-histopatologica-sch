# Ontologia Histopatologica SCH

Ontología Histopatológica del Sistema Cardiovascular Humano
Resumen: El propósito de este documento es presentar como contribución a la ontología histológica del sistema
cardiovascular humano (HO), una ontología histopatológica en relación a este sistema. Esta propuesta servirá como apoyo a
estrategias de enseñanza y el análisis de muestras histológicas en el estudio de la histopatología. En este documento son
presentados el problema, objetivos, actividades a seguir y los resultados esperados, entre otros con el fin de mostrar una idea
clara acerca de este proyecto.